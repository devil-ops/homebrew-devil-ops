# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Cartographerctl < Formula
  desc "Cartographer CLI"
  homepage "https://gitlab.oit.duke.edu/devil-ops/cartographerctl"
  version "0.1.0"

  on_macos do
    on_intel do
      url "https://gitlab.oit.duke.edu/devil-ops/cartographerctl/-/releases/v0.1.0/downloads/cartographerctl-0.1.0_darwin_amd64.tar.gz"
      sha256 "b600568b27cb19f1ae356a3b8251d0d599abd590eb15944742e1085c0eb7ac67"

      def install
        bin.install "cartographerctl"
      end
    end
    on_arm do
      url "https://gitlab.oit.duke.edu/devil-ops/cartographerctl/-/releases/v0.1.0/downloads/cartographerctl-0.1.0_darwin_arm64.tar.gz"
      sha256 "6fa4d8187f2da066eaa107b5027c58b2218284026e42fc9ac7e42a1cdafe3fd5"

      def install
        bin.install "cartographerctl"
      end
    end
  end

  on_linux do
    on_intel do
      if Hardware::CPU.is_64_bit?
        url "https://gitlab.oit.duke.edu/devil-ops/cartographerctl/-/releases/v0.1.0/downloads/cartographerctl-0.1.0_linux_amd64.tar.gz"
        sha256 "14ef27dd6a399e8bebb26e832c94d575f772225432345c0f937b4428374df08a"

        def install
          bin.install "cartographerctl"
        end
      end
    end
    on_arm do
      if Hardware::CPU.is_64_bit?
        url "https://gitlab.oit.duke.edu/devil-ops/cartographerctl/-/releases/v0.1.0/downloads/cartographerctl-0.1.0_linux_arm64.tar.gz"
        sha256 "dec129f644a96af061955aecd99aacff8713dd05c777cce79f6aca5705c89f65"

        def install
          bin.install "cartographerctl"
        end
      end
    end
  end
end
