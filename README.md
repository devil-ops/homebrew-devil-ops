# homebrew-devil-ops

Homebrew TAP for misc. Devil-ops packages

To use this repository add it with:

```bash
$ brew tap devil-ops/devil-ops https://gitlab.oit.duke.edu/devil-ops/homebrew-devil-ops.git
```


## Notes

Originally this tap was hosted on the `master` branch, because of a hard coding
in the `goreleaser` tool. This has been changed, but users will need to untap
and retap to get things going again:

```
$ brew untap devil-ops/devil-ops
$ brew tap devil-ops/devil-ops https://gitlab.oit.duke.edu/devil-ops/homebrew-devil-ops.git
```
