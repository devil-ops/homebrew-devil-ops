# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Apiaryctl < Formula
  desc "Golang utility [apiaryctl]"
  homepage "https://gitlab.oit.duke.edu/devil-ops/go-apiary"
  version "0.1.2"
  license "MIT"

  on_macos do
    if Hardware::CPU.arm?
      url "https://gitlab.oit.duke.edu/devil-ops/go-apiary/-/releases/v0.1.2/downloads/go-apiary-0.1.2_darwin_arm64.tar.gz"
      sha256 "cccd3726fcf073bdeb92291efd53b4776b36e5e50419c06bd361450fd7c22def"

      def install
        bin.install "apiaryctl"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.oit.duke.edu/devil-ops/go-apiary/-/releases/v0.1.2/downloads/go-apiary-0.1.2_darwin_amd64.tar.gz"
      sha256 "49871c32dd4b59ba575ff3bbe147de1331793c53c01039caca415b8324209772"

      def install
        bin.install "apiaryctl"
      end
    end
  end

  on_linux do
    if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
      url "https://gitlab.oit.duke.edu/devil-ops/go-apiary/-/releases/v0.1.2/downloads/go-apiary-0.1.2_linux_arm64.tar.gz"
      sha256 "20c68a8374287156c53c5ea028005aeea66e8b6aa3bb5f4fd491a0d6989eb8bc"

      def install
        bin.install "apiaryctl"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.oit.duke.edu/devil-ops/go-apiary/-/releases/v0.1.2/downloads/go-apiary-0.1.2_linux_amd64.tar.gz"
      sha256 "324e917fb9b1c9b16b0da1489cbb5400c3301b35eda8fb27056aa59b2a3762d8"

      def install
        bin.install "apiaryctl"
      end
    end
  end
end
