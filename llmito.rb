# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Llmito < Formula
  desc "Interact with LiteLLM Services"
  homepage "https://gitlab.oit.duke.edu/devil-ops/llmito"
  version "0.1.4"

  on_macos do
    url "https://gitlab.oit.duke.edu/devil-ops/llmito/-/releases/v0.1.4/downloads/llmito-0.1.4_darwin_amd64.tar.gz"
    sha256 "83fafad03cd75727bdae19e5d9324231903b671475f23bb6be7f84188c869f86"

    def install
      bin.install "llmito"
    end

    on_arm do
      def caveats
        <<~EOS
          The darwin_arm64 architecture is not supported for the Llmito
          formula at this time. The darwin_amd64 binary may work in compatibility
          mode, but it might not be fully supported.
        EOS
      end
    end
  end

  on_linux do
    on_intel do
      if Hardware::CPU.is_64_bit?
        url "https://gitlab.oit.duke.edu/devil-ops/llmito/-/releases/v0.1.4/downloads/llmito-0.1.4_linux_amd64.tar.gz"
        sha256 "5878714338692babdf03506b618314600bedc65b53c79a4884a4efd197289a13"

        def install
          bin.install "llmito"
        end
      end
    end
  end
end
