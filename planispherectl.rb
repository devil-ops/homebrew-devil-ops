# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Planispherectl < Formula
  desc "Planisphere CLI"
  homepage "https://gitlab.oit.duke.edu/devil-ops/planispherectl"
  version "0.6.1"

  on_macos do
    if Hardware::CPU.arm?
      url "https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/releases/v0.6.1/downloads/planispherectl_0.6.1_darwin_arm64.tar.gz"
      sha256 "d36c030e66ac0341747d80a5a8f74f36f01d349f1827ed07d0111fd13656a043"

      def install
        bin.install "planispherectl"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/releases/v0.6.1/downloads/planispherectl_0.6.1_darwin_amd64.tar.gz"
      sha256 "3b99efb69215eaa015818d7858afc4ba92b2c0bc5e667ed4320e8baea10a32ac"

      def install
        bin.install "planispherectl"
      end
    end
  end

  on_linux do
    if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
      url "https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/releases/v0.6.1/downloads/planispherectl_0.6.1_linux_arm64.tar.gz"
      sha256 "c7a95e8cc544fff5d6bc84314f62bbb4bedb074e7a91419ce7ae4225cb9770ff"

      def install
        bin.install "planispherectl"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/releases/v0.6.1/downloads/planispherectl_0.6.1_linux_amd64.tar.gz"
      sha256 "19748b94e7d5a693d7cab8921a48e7b534b704a884decf37a81b74ef614ceeae"

      def install
        bin.install "planispherectl"
      end
    end
  end
end
